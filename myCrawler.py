# import urllib.request
from PIL import Image
import requests
import os
import re
import csv
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from fpdf import FPDF
from dotenv import load_dotenv

csvfile='book_data.csv'
savePath="save/"
saveGallery="Book-"


downloaded="Downloaded "
codeList=[]

def GrabDigit(mystr,keyword,offset,stopKeyword):
	index=mystr.index(keyword)
	i=index+len(keyword)+offset
	code=""
	while(mystr[i].isdigit()):
		code=code+mystr[i]
		i=i+1
	return code

def GrabStuff(mystr,keyword,offset,stopKeyword):
	index=mystr.index(keyword)
	i=index+len(keyword)+offset
	code=""
	while(mystr[i]!=stopKeyword):
		code=code+mystr[i]
		i=i+1
	return code


def GrabHTML(url):
	headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
	result = requests.get(url, headers=headers)

	mystr= result.content.decode()
	return mystr

def crop(infile,height,width):
    im = Image.open(infile)
    imgwidth, imgheight = im.size
    for i in range(imgheight//height):
        for j in range(imgwidth//width):
            box = (j*width, i*height, (j+1)*width, (i+1)*height)
            im.crop(box).save(infile.split(".")[0]+"_"+str(j)+".png")

def WriteFile(path,content):
	with open(path,"w") as f:
		f.write(content)
		f.close()

def DownloadPng(url,path):
	# assume it must fall to one of the type
	extensions=[".png",".jpg",".tif"]

	for extention in extensions:
		r=requests.get(url+extention)
		if(r.status_code!=404):
			img_data=r.content
			break

	with open(path+extention, 'wb') as handler:
		handler.write(img_data)



def CombineTwoPicture(patha,pathb,outpath):
	scale=1
	if(os.path.exists(patha) and os.path.exists(pathb)):
		background = Image.open(patha)
		overlay = Image.open(pathb)

		background = background.convert("RGBA")
		overlay = overlay.convert("RGBA")

		background=background.resize(overlay.size,Image.ANTIALIAS)
		background.paste(overlay, (0,0), overlay)
		background=background.resize((int(background.size[0]*scale),int(background.size[1]*scale)),Image.ANTIALIAS)
		background.save(outpath,"PNG")

	elif(os.path.exists(patha)):
		Image.open(patha).save(outpath,"PNG")

	elif(os.path.exists(pathb)):
		Image.open(pathb).save(outpath,"PNG")



def GetBook(bookCode,bookFolderCode,bookPageCode,pages,uni):
	book = bookCode
	filename = bookCode
	folderTemp=book+"_Temp/"
	folderFinal=book+"/"
	imagelist=[]
	print("--- Getting Book : "+book+" ---")
	if (not os.path.exists(folderTemp)):
		os.makedirs(folderTemp)
	if (not os.path.exists(folderFinal)):
		os.makedirs(folderFinal)

	for i in range(1,pages+1):
		savePath_bg=folderTemp+filename+"_P"+str(i)+"_BG"
		savePath_text=folderTemp+filename+"_P"+str(i)+"_Text"
		savePath_final=folderFinal+filename+"_P"+str(i)
		if(not os.path.exists(savePath_bg+".png")):
			print("-Downloading Page: "+str(i)+"/"+str(pages)+"\t  ("+savePath_bg+".png)")
			DownloadPng(os.getenv('CrawlPathHeader')+bookFolderCode+"/files/assets/common/page-html5-substrates/page"+str(i).zfill(4)+"_"+bookPageCode+".jpg?uni="+uni,savePath_bg)
		if(not os.path.exists(savePath_text+".png")):
			print("-Downloading Page: "+str(i)+"/"+str(pages)+"\t  ("+savePath_text+".png)")
			DownloadPng(os.getenv('CrawlPathHeader')+bookFolderCode+"/files/assets/common/page-textlayers/page"+str(i).zfill(4)+"_"+bookPageCode+".png?uni="+uni,savePath_text)
			
		# Combine two page
		if(not os.path.exists(savePath_final+".png")):
			print("-Combining Page: "+str(i)+"/"+str(pages)+"\t  ("+savePath_final+".png)")
			CombineTwoPicture(savePath_bg+".png",savePath_text+".png",savePath_final+".png")

		imagelist.append(savePath_final+".png")
		print("Finishing Page: "+str(i)+"/"+str(pages)+"\t  ("+savePath_final+".png)")
	return imagelist

def ToPDF(imagelist,path):
	print("--- Generating PDF "+path+" ---")
	pdf = FPDF()
	# imagelist is the list with all image filenames
	scale=210

	for image in imagelist:
		
		print("Generating Image [total "+str(len(imagelist))+"]:"+image+"")
		# Do if split image into two, save, and put two in pdf
		img=Image.open(image)
		if(img.size[0]>img.size[1]):
			# The image is 2 page
			print("Generating Image [total "+str(len(imagelist))+"]:"+image+"  2nd page ")
			crop(image,img.size[1],int(img.size[0]/2))
			pdf.add_page()
			pdf.image(image.split(".")[0]+"_0.png",0,0,scale)
			pdf.add_page()
			pdf.image(image.split(".")[0]+"_1.png",0,0,scale)
		else:
			pdf.add_page()
			pdf.image(image,0,0,scale)
		

	pdf.output(path, "F")

def DownloadBook(bookData,currentBook):
	uni = bookData[currentBook][0]
	pages = bookData[currentBook][1]
	bookCode= bookData[currentBook][2]
	bookFolderCode = bookData[currentBook][3]
	bookPageCode= bookData[currentBook][4]

	imagelist=GetBook(bookCode,bookFolderCode,bookPageCode,pages,uni)
	ToPDF(imagelist,bookCode+".pdf")

def DownloadAllBook(bookData):
	print("--- Start Searching Book ---")
	currentBook=0
	while(os.path.exists(bookData[currentBook][2]+"_Temp")):
		currentBook+=1
		if(currentBook>=len(bookData)):
			print("--- Finishing all Download ---")
			return
		print("Skipping book : "+bookData[currentBook][2])

	print("--- Start Downloading Book "+bookData[currentBook][2]+" with pages "+str(bookData[currentBook][1])+" ---")
	DownloadBook(bookData, currentBook)

bookData=[
	("hashCode",36,"bookCode","bookFolder","PageCode"),
]


load_dotenv()
DownloadAllBook(bookData)


